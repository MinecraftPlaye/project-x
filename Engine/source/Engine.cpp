#include "include\Engine.h"

#ifdef _WIN64
LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);

void initOpenGL(HWND windowHandle);

int CALLBACK wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow) {
    // declare the window class and clear the memory of it
    WNDCLASSEX windowClass = { 0 };
    
    windowClass.cbSize = sizeof(WNDCLASSEX);
    // TODO: do CS_OWNDC & CS_HREDRAW & CS_VREDRAW still matter?
    windowClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowProc;
    windowClass.hInstance = hInstance;
    windowClass.hIcon = NULL;
    windowClass.hIconSm = NULL;
    windowClass.lpszClassName = L"DarkEngine Window";

    if (!RegisterClassEx(&windowClass)) {
        // TODO: logging
        return GetLastError();
    }

    HWND windowHandle = CreateWindowEx(0, windowClass.lpszClassName, L"Window Name",
        WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE, 
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
        0, 0, hInstance, 0);

    if (windowHandle == NULL) {
        // TODO: logging
        return GetLastError();
    }

    HDC deviceContext = CreateCompatibleDC(0);

    initOpenGL(windowHandle);

    for (;;) {
        MSG message;
        bool msgResult = GetMessage(&message, 0, 0, 0);
        if (msgResult > 0) {
            TranslateMessage(&message);
            DispatchMessage(&message);
        } else {
            break;
        }
    }

    ReleaseDC(0, deviceContext);
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    LRESULT result{ 0 };

    switch (uMsg) {
	  case WM_SIZE: {
		  break;
	  }
	  case WM_DESTROY: {
          PostQuitMessage(0);
		  break;
	  }
	  case WM_CLOSE: {
          PostQuitMessage(0);
		  break;
	  }
	  case WM_ACTIVATEAPP: {
		  break;
	  }
	  default: {
          result = DefWindowProc(hwnd, uMsg, wParam, lParam);
		  break;
	  }
    }

    return result;
}

void initOpenGL(HWND windowHandle) {
    HDC dc = GetDC(windowHandle);

    PIXELFORMATDESCRIPTOR pfd{
        sizeof(PIXELFORMATDESCRIPTOR), 1,
        PFD_DRAW_TO_WINDOW |PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
        PFD_TYPE_RGBA, 32,
        // color bits ignored
        0, 0, 0, 0, 0, 0,
        // no alpha buffer & shift bit ignored
        8, 0,
        // accum bits ignored
        0, 0, 0, 0, 0,
        // depth and stencil
        16, 0,
        0, PFD_MAIN_PLANE, 0, 0, 0, 0
     };

    int iPixelFormat{ ChoosePixelFormat(dc, &pfd) };
    PIXELFORMATDESCRIPTOR suggestedPfd;
    DescribePixelFormat(dc, iPixelFormat, sizeof(suggestedPfd), &suggestedPfd);
    SetPixelFormat(dc, iPixelFormat, &suggestedPfd);

    HGLRC openGLRC = wglCreateContext(dc);

    if (!wglMakeCurrent(dc, openGLRC)) {
        // TODO: logging
    }

    // TODO: enable OpenGL 3.0+ support via extension
    HGLRC wglCreateContextAttribsARB = wglGetProcAddress("");

    wglDeleteContext(openGLRC);
}

#elif defined _LINUX
#endif