#ifndef _MAIN_H_
#define _MAIN_H_

/*
    Windows specific
*/
#ifdef _WIN64
    // enable Windows Unicode support
    #ifndef UNICODE
    #define UNICODE
    #endif

    #include <Windows.h>

/*
    Linux specific
*/
#elif defined _LINUX
    #include <X11/Xlib.h>
#endif /* Platform specific code */

// this has to be included AFTER Windows.h as it redefines some of these functions
#include <gl/GL.h>

#endif /* _MAIN_H_ */