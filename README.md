# Project X

## Branches
Development takes place in the "development" branch. The "stabilization" branch is used for fixing bugs
and pre-releases.

The "master" branch is the main branch of the repository and used for releases.
